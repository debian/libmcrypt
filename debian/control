Source: libmcrypt
Section: libs
Priority: optional
Maintainer: Barak A. Pearlmutter <bap@debian.org>
Build-Depends: debhelper-compat (= 13), libltdl-dev,
		autoconf-archive,
		pkgconf,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libmcrypt
Vcs-Git: https://salsa.debian.org/debian/libmcrypt.git
Homepage: https://mcrypt.sourceforge.net/

Package: libmcrypt4
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libmcrypt-dev, mcrypt
Multi-Arch: same
Description: De-/Encryption Library
 libmcrypt is the library which implements all the algorithms and
 modes found in mcrypt.
 .
 libmcrypt supports the algorithms: BLOWFISH, TWOFISH, DES, TripleDES,
 3-WAY, SAFER-sk64, SAFER-sk128, SAFER+, LOKI97, GOST, RC2, RC6, MARS,
 IDEA, RIJNDAEL-128, RIJNDAEL-192, SERPENT, RIJNDAEL-256, CAST-128
 (known as CAST5), CAST-256, ARCFOUR, ENIGMA, PANAMA, XTEA and WAKE.
 Block algorithms can be used in: CBC, ECB, CFB and OFB (8 bit and n
 bit, where n is the size of the algorithm's block length).
 .
 More information can be found at the libmcrypt homepage
 https://mcrypt.sourceforge.net/ .

Package: libmcrypt-dev
Section: libdevel
Architecture: any
Depends: libmcrypt4 (= ${binary:Version}), ${misc:Depends}
Description: De-/Encryption Library development files
 libmcrypt is the library which implements all the algorithms and
 modes found in mcrypt.
 .
 libmcrypt supports the algorithms: BLOWFISH, TWOFISH, DES, TripleDES,
 3-WAY, SAFER-sk64, SAFER-sk128, SAFER+, LOKI97, GOST, RC2, RC6, MARS,
 IDEA, RIJNDAEL-128, RIJNDAEL-192, SERPENT, RIJNDAEL-256, CAST-128
 (known as CAST5), CAST-256, ARCFOUR, ENIGMA, PANAMA, XTEA and WAKE.
 Block algorithms can be used in: CBC, ECB, CFB and OFB (8 bit and n
 bit, where n is the size of the algorithm's block length).
 .
 More information can be found at the libmcrypt homepage
 https://mcrypt.sourceforge.net/ .
